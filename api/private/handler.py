from api.utils.Response import Response

def private_endpoint(event, context):
    return Response._200(body=dict(message='Private Endpoint!'))