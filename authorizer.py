from jose import jwk, jwt

from api.utils.Response import Response
from api.utils.printers import format_error
# from cryptography.hazmat.backends import default_backend
# from cryptography.x509 import load_pem_x509_certificate

PREFIX = 'Bearer'

def auth(event, context):
    try:
        whole_token = event.get('authorizationToken')

        if not whole_token:
            # C-401-U1
            format_error("NO TOKEN", "C-401-U1")
            return Response._401()

        token = whole_token.split(' ')[1]

        # get the last two sections of the token,
        # message and signature (encoded in base64)
        _, encoded_signature = str(token).rsplit('.', 1)

        # decode the signature
        # decoded_signature = base64url_decode(encoded_signature.encode('utf-8'))

        # C-401-U4
        # use the unverified claims
        claims = jwt.get_unverified_claims(token)

        # now we can use the claims
        policy = generate_policy(claims['sub'], 'Allow', event['methodArn'])

        return policy
    except Exception as e:
        # C-401-U7
        format_error("Fatal Error in authorizer.py: {}".format(e), "C-401-U7")
        return Response._500()
    
def generate_policy(principal_id, effect, resource):
    return {
        'principalId': principal_id,
        'policyDocument': {
            'Version': '2012-10-17',
            'Statement': [
                {
                    "Action": "execute-api:Invoke",
                    "Effect": effect,
                    "Resource": resource
                }
            ]
        }
    }