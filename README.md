# AWS Python HTTP API with JWT Authorizer

## Installation

Install node and sls dependencies.

```sh
sls plugin install -n serverless-python-requirements
sls plugin install -n serverless-offline
```

Create python3.X virtualenv, activate and install dependencies.

```sh
virtualenv .venv
.venv\Scripts\activate (Windows)
pip install -r requirements-v1.txt
```

Start offline server.

```sh
sls offline
```


Request to http://localhost:8080/ or  any endpoint HTTP with the Bearer JWT:

- dev/api/private (Take the JWT of jwt-text)
- dev/api/public (Optional JWT)
